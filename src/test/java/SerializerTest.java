import org.apache.log4j.Logger;
import org.junit.*;
import org.junit.rules.TemporaryFolder;

import java.io.*;
import java.util.Arrays;

public class SerializerTest extends Assert {

    private static Logger logger = Logger.getLogger(SerializerTest.class);

    static private File resultFirstUserXML;
    static private File resultSecondUserXML;
    static private File resultFirstUserJSON;
    static private File resultSecondUserJSON;

    static private File referenceFirstUserXML;
    static private File referenceSecondUserXML;
    static private File referenceFirstUserJSON;
    static private File referenceSecondUserJSON;

    static private SerializerXML serializerXML = new SerializerXML();
    static private SerializerJSON serializerJSON = new SerializerJSON();

    static private String baseReferencePath = "src/test/resources/reference/";

    static private User<String> adminUser = new User<>();
    static private User<String> operatorUser = new User<>();

    private User<String> resultAdminUser = new User<>();
    private User<String> resultOperatorUser = new User<>();

    @ClassRule
    public static TemporaryFolder folder = new TemporaryFolder();

    @BeforeClass
    public static void createFiles(){

        logger.debug("Test started");

        try {
            resultFirstUserXML = folder.newFile();
            resultSecondUserXML = folder.newFile();
            resultFirstUserJSON = folder.newFile();
            resultSecondUserJSON = folder.newFile();

            logger.debug("result files are created");
        } catch (IOException e) {
            logger.error("error create file: ", e);
        }

        referenceFirstUserXML = new File(baseReferencePath + "firstUser.xml");
        referenceSecondUserXML = new File(baseReferencePath + "secondUser.xml");
        referenceFirstUserJSON = new File(baseReferencePath + "firstUser.json");
        referenceSecondUserJSON = new File(baseReferencePath + "secondUser.json");

        logger.debug("reference files are created");

        adminUser.write("admin", "Kest", "1", new AdminLogger(), Arrays.asList("folder1", "folder2", "folder3"), null);
        operatorUser.write("operator", "Smite", "2", new OperatorLogger(), Arrays.asList("folder1", "folder2"), adminUser);

        logger.debug("users are created");
    }

    @Test
    public void testSerializeToXML() {

        serializerXML.serialization(adminUser, resultFirstUserXML);
        serializerXML.serialization(operatorUser, resultSecondUserXML);

        logger.debug("objects are serialized to XML");

        assertTrue(fileCompare(referenceFirstUserXML, resultFirstUserXML));
        assertTrue(fileCompare(referenceSecondUserXML, resultSecondUserXML));
    }

    @Test
    public void shouldUnSerializeToUserFromXML() {
        resultAdminUser = serializerXML.unSerialization(referenceFirstUserXML);
        resultOperatorUser = serializerXML.unSerialization(referenceSecondUserXML);

        logger.debug("objects are unserialized from XML");

        assertTrue(Serializer.compare(adminUser, resultAdminUser));
        assertTrue(Serializer.compare(operatorUser, resultOperatorUser));
    }

    @Test
    public void testSerializeToJSON(){
        serializerJSON.serialization(adminUser, resultFirstUserJSON);
        serializerJSON.serialization(operatorUser, resultSecondUserJSON);

        logger.debug("objects are serialized to JSON");

        assertTrue(fileCompare(referenceFirstUserJSON, resultFirstUserJSON));
        assertTrue(fileCompare(referenceSecondUserJSON, resultSecondUserJSON));
    }

    @Test
    public void shouldUnSerializeToUserFromJSON() {
        resultAdminUser = serializerJSON.unSerialization(referenceFirstUserJSON);
        resultOperatorUser = serializerJSON.unSerialization(referenceSecondUserJSON);

        logger.debug("objects are unserialized from JSON");

        assertTrue(Serializer.compare(adminUser, resultAdminUser));
        assertTrue(Serializer.compare(operatorUser, resultOperatorUser));
    }

    public boolean fileCompare(File a, File b){
        String strA = fileRead(a);
        String strB = fileRead(b);
        return strA.equals(strB);
    }

    public String fileRead(File a){
        int c;

        StringBuffer strA = new StringBuffer();

        try (FileReader reader = new FileReader(a)){
            while ((c=reader.read())!=-1) {
                strA.append((char) c);
            }
        } catch (FileNotFoundException e) {
            logger.error(a + " not found: ", e);
        } catch (IOException e) {
            logger.error("IOException from " + a + ": ", e);
        }

        return strA.toString();
    }
}
