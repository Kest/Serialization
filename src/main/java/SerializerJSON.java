import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;
import com.thoughtworks.xstream.io.json.JsonHierarchicalStreamDriver;

import java.io.*;

public class SerializerJSON extends Serializer {

    @Override
    public void serialization(User user, File file) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            XStream xstream = new XStream(new JsonHierarchicalStreamDriver());
            xstream.setMode(XStream.NO_REFERENCES);
            xstream.alias("user", User.class);
            xstream.processAnnotations(User.class);
            String json = xstream.toXML(user);
            fileOutputStream.write(json.getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public User unSerialization(File file) {
        XStream xstream = new XStream(new JettisonMappedXmlDriver());
        xstream.setMode(XStream.NO_REFERENCES);
        xstream.alias("user", User.class);
        xstream.processAnnotations(User.class);
        Object o = xstream.fromXML(file);
        return (User) o;
    }
}
