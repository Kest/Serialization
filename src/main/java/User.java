import com.thoughtworks.xstream.annotations.*;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlType(propOrder = { "id", "name", "eventLogger", "folders", "creator" })
@XmlSeeAlso({AdminLogger.class, OperatorLogger.class})
@XmlRootElement
public class User<T> {
    @lombok.Getter
    @lombok.Setter
    private T id;
    @lombok.Getter
    @lombok.Setter
    private String name;
    //Не сериализуем
    @XStreamOmitField
    private String password;
    @lombok.Getter
    @lombok.Setter
    private EventLogger eventLogger;
    @lombok.Getter
    @lombok.Setter
    @XStreamImplicit
    private List<String> folders;
    @lombok.Getter
    @lombok.Setter
    private User creator;

    public void write(T id, String name, String password, EventLogger eventLogger, List<String> folders, User creator){
        this.id = id;
        this.name = name;
        this.password = password;
        this.eventLogger = eventLogger;
        this.folders = folders;
        this.creator = creator;
    }

}