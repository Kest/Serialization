import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;

public class SerializerXML extends Serializer {

    @Override
    void serialization(User user, File file) {
        try {
            JAXBContext context = JAXBContext.newInstance(User.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            FileOutputStream fileInputStream = new FileOutputStream(file);
            marshaller.marshal(user, fileInputStream);
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    User unSerialization(File file) {
        try {
            JAXBContext context =
                    null;
            try {
                context = JAXBContext.newInstance(User.class);
            } catch (JAXBException e) {
                e.printStackTrace();
            }
            FileInputStream fileInputStream = new FileInputStream(file);
            Unmarshaller unmarshaller =
                    context.createUnmarshaller();
            Object o = unmarshaller.unmarshal(fileInputStream);
            User userCopy = (User) o;
            return userCopy;

        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
