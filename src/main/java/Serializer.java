import java.io.File;

abstract class Serializer {

    abstract void serialization(User user, File file);
    abstract User unSerialization(File file);

    public static boolean compare(User a, User b){

        boolean name = a.getName().equals(b.getName());

        boolean id = a.getId().equals(b.getId());

        Class<?> myClass = a.getEventLogger().getClass();
        boolean logger = myClass.isInstance(b.getEventLogger());

        boolean folders = false;

        if (a.getFolders()!=null){
            if (b.getFolders()!=null){
                folders = a.getFolders().equals(b.getFolders());
            }
        }else {
            if (b.getFolders() == null)
                folders = true;
        }

        boolean creator = false;
        if (a.getCreator()!=null){
            if (b.getCreator()!=null){
                creator = Serializer.compare(a.getCreator(), b.getCreator());
            }
        }else {
            if (b.getCreator() == null)
                creator = true;
        }

        return name && id && logger && folders && creator;
    }
}
